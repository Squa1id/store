<?php

class Application_Model_DbTable_Products extends Zend_Db_Table_Abstract
{
    protected $_name = 'products';
    public function getProductByID($id)
    {
        $data = $this->fetchRow('id = ' . $id);
        return $data;
    }

    public function getProductByURL($url)
    {
        $data = $this->fetchRow('url_key = ' . $url);
        return $data;
    }

    public function getProductsByID($id)
    {
        $data = $this->fetchAll('id_category = ' . $id)->toArray();
        return $data;
    }
}
