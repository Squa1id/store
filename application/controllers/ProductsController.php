<?php

class ProductsController extends Zend_Controller_Action
{
    public function init()
    {
        /* Initialize action controller here */
    }

    public function indexAction()
    {
        $categories = new Application_Model_DbTable_Categories();
        $categories = $categories->fetchAll()->toArray();
        $this->view->categories = $categories;
    }

    public function categoryAction()
    {
        $category = new Application_Model_DbTable_Categories();
        $category = $category->getIDByURL($this->_getParam('url'));
        $id = $category['id'];
        $this->view->category_name = $category['name'];
        $products = new Application_Model_DbTable_Products();
        $products = $products->getProductsByID($id);
        $this->view->products = $products;
    }

    public function productAction()
    {
        $product = new Application_Model_DbTable_Products();
        $product = $product->getProductByURL($this->_getParam('url'))->toArray();
        $this->view->product = $product;
    }


}





