<?php

class IndexController extends Zend_Controller_Action
{

    public function init()
    {
        /* Initialize action controller here */
    }

    public function indexAction()
    {
        $products = new Application_Model_DbTable_Products();
        $products = $products->fetchAll()->toArray();
        $categories = new Application_Model_DbTable_Categories();
        $i=0;
        while ($i != count($products))
        {
            $name_category = ($categories->fetchRow('id = '.$products[$i]['id_category'])->toArray())['name'];
            $products[$i]['name_category']= $name_category;
            $i++;
        }
        $this->view->products = $products;
    }


}

