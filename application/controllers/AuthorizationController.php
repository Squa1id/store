<?php

class AuthorizationController extends Zend_Controller_Action
{

    public function init()
    {
        /* Initialize action controller here */
    }

    public function loginAction()
    {
        $this->view->form = new Application_Form_Login();
    }

    public function logoutAction()
    {
        // action body
    }

    public function registrationAction()
    {
        $this->view->form = new Application_Form_Registration();
    }

    public function createuserAction()
    {
        $login = filter_input(INPUT_POST,'login',FILTER_SANITIZE_MAGIC_QUOTES);
        $password = password_hash($_POST['password'], PASSWORD_DEFAULT);
        $name = filter_input(INPUT_POST,'name',FILTER_SANITIZE_MAGIC_QUOTES);
        $surname = filter_input(INPUT_POST,'surname',FILTER_SANITIZE_MAGIC_QUOTES);
        $email = filter_input(INPUT_POST,'email',FILTER_SANITIZE_EMAIL);
        $db = new Application_Model_DbTable_Users();
        if($db->Check_Login($login))
        {
            $user = array(
                'login' => $login,
                'password' => $password,
                'name' => $name,
                'surname' => $surname,
                'email' => $email
            );
            $db->insert($user);
        }
        $this->redirect('/');
    }


}







