<?php

class Application_Form_Registration extends Zend_Form
{
    public function init()
    {
        $this->setMethod(self::METHOD_POST);
        $this->setAction('/authorization/createuser');
        $this->addElement('text', 'name', array(
            'label' => 'Имя',
            'required' => true,
            'validators' => array(
                array('StringLength', false, array('max'=>75)),
            ),
        ));
        $this->addElement('text', 'surname', array(
            'label' => 'Фамилия',
            'required' => true,
            'validators' => array(
                array('StringLength', false, array('max'=>75)),
            ),
        ));
        $this->addElement('text', 'email', array(
            'label' => 'Электронная почта',
            'required' => true,
            'validators' => array(
                array('StringLength', false, array('max'=>75)),
                'EmailAddress',
            ),
        ));
        $this->addElement('text', 'login', array(
            'label' => 'Логин',
            'required' => true,
            'validators' => array(
                array('StringLength', false, array('max'=>75)),
            ),
        ));
        $this->addElement('text', 'password', array(
            'label' => 'Пароль',
            'required' => true,
            'validators' => array(
                array('StringLength', false, array('max'=>75)),
            ),
        ));
        $this->addElement('submit', 'go', array(
            'label' => 'Зарегистрироваться',
        ));
    }


}

