<?php

class Application_Form_Login extends Zend_Form
{

    public function init()
    {
        $this->setMethod(self::METHOD_POST);
        $this->setAction('/authorization/');
        $this->addElement('text', 'login', array(
            'label' => 'Логин',
            'required' => true,
            'validators' => array(
                array('StringLength', false, array('max'=>75)),
            ),
        ));
        $this->addElement('text', 'password', array(
            'label' => 'Пароль',
            'required' => true,
            'validators' => array(
                array('StringLength', false, array('max'=>75)),
            ),
        ));
        $this->addElement('submit', 'go', array(
            'label' => 'Войти',
        ));
    }


}

